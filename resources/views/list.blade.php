<html>
    <head>
    <title>Pendaftaran</title>
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    </head>
    <body>
        
        <div class="container-fluid mt-4">
            <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama lengkap</th>
                <th scope="col">Tempat lahir</th>
                <th scope="col">Tanggal lahir</th>
                <th scope="col">Jenis Kelamin</th>
                <th scope="col">Alamat</th>
                <th scope="col">E-mail</th>
                <th scope="col">No.hp</th>
                <th scope="col">kota</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $item) 
            <tr>
                <th scope="row">{{ $loop->iteration }}</th>
                <td>{{$item->nama}}</td>
                <td>{{$item->tempat_lahir}}</td>
                <td>{{$item->tanggal_lahir}}</td>
                <td>{{$item->jenis_kelamin}}</td>
                <td>{{$item->alamat}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->no_hp}}</td>
                <td>{{$item->kota}}</td>
                <td>
                    <a href="{{ route('edit', $item->id) }}">
                        <button type="button" class="btn btn-success btn-sm">Edit</button>
                    </a>
                    <form method="POST" action="{{ route('delete', $item->id) }}" id="hapus">
                        @csrf
                        @method('DELETE')
                        <div class="d-grid gap-2">
                        <button type="submit" class="btn btn-danger" onclick="return confirm('yakin?')">Hapus</button>
                        </div>
                    </form>            
                </td>
                
            </tr>
            @endforeach
            </tbody>
            </table>
        </div>
    

    </body>    
</html>