<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gym extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama',
        'tempat_lahir' ,
        'tanggal_lahir' ,
        'jenis_kelamin' ,
        'alamat' ,
        'email' ,
        'no_hp' ,
        'kota'

    ];
}
