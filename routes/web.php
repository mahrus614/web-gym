<?php

use App\Http\Controllers\GymController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


route::get('/list', [GymController::class, 'index'])->name('list');
route::get('/tambah' , [GymController::class , 'create'])->name('tambah');
route::post('/tambah' , [GymController::class , 'store'])->name('tambah');
route::get('/edit/{id}' , [GymController::class , 'edit'])->name('edit');
route::put('/edit/{id}' , [GymController::class , 'update'])->name('edit');
route::delete('/delete/{id}' , [GymController::class , 'destroy'])->name('delete');


